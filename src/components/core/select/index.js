import { printWarning } from '~/utils/handle-error';
import { PREFIX } from '~/utils/configs';

const COMPONENT_CLASS = PREFIX + 'select';

export default {
  name: COMPONENT_CLASS,

  inheritAttrs: false,

  model: {
    props: 'value',
    event: 'input'
  },

  props: {
    tag: {
      type: String,
      default: 'div'
    },
    value: {
      type: [String, Number],
      default: null
    },
    options: {
      type: Array, // [{ text: String, value: String | Number | Boolean }]
      default: () => []
    },
    disabled: Boolean,
    readonly: Boolean
  },

  data() {
    return {
      localValue: this.value
    };
  },

  computed: {
    classes() {
      return {
        [`${COMPONENT_CLASS}--disabled`]: this.disabled,
        [`${COMPONENT_CLASS}--readonly`]: this.readonly
      };
    },

    selectListeners() {
      // `Object.assign` merges objects together to form a new object
      return Object.assign(
        {},
        // We add all the listeners from the parent
        this.$listeners,
        // Then we can add custom listeners or override the
        // behavior of some listeners.
        {
          input: this.handleInput // This ensures that the component works with v-model
        }
      );
    }
  },

  watch: {
    value(newValue) {
      this.localValue = newValue;
    },

    localValue(newValue) {
      this.$emit('input', newValue);
    }
  },

  methods: {
    handleInput(event) {
      this.localValue = event.target.value;
    }
  },

  render(h) {
    const createOption = props =>
      h(
        'option',
        {
          attrs: props,
          domProps: {
            value: props.value
          }
        },
        props.text
      );

    const select = h(
      'select',
      {
        class: `${COMPONENT_CLASS}__select`,
        attrs: {
          ...this.$attrs,
          disabled: this.disabled,
          readonly: this.readonly
        },
        domProps: {
          value: this.localValue
        },
        on: this.selectListeners
      },
      [this.options.map(opt => createOption(opt))]
    );

    const arrow = h('span', {
      class: `${COMPONENT_CLASS}__arrow`
    });

    return h(
      this.tag,
      {
        class: [COMPONENT_CLASS, this.classes]
      },
      [select, arrow]
    );
  }
};
