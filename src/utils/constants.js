export const THEME_VARIANT_LIST = [
  'primary',
  'secondary',
  'success',
  'danger',
  'warning',
  'info',
  'dark',
  'light'
];

export const COMPONENT_SIZE_LIST = ['md', 'sm', 'lg'];

export const PRIORITY_OPTS = [
  {
    value: 0,
    text: 'Low'
  },
  {
    value: 1,
    text: 'Normal'
  },
  {
    value: 2,
    text: 'High'
  }
];

export const DEFAULT_PRIORITY = 1;
