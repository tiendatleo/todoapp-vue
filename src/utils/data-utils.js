import { LOCAL_STORAGE_KEY } from '~/utils/configs';

export const getTodoListFromLocalStorage = () => {
  const list = window.localStorage.getItem(LOCAL_STORAGE_KEY);
  return list ? JSON.parse(list) : null;
};

export const setTodoListToLocalStorage = list => {
  window.localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(list));
};
