// Global configs for project

export const PREFIX = 'v-';

export const DATE_FORMAT = 'DD/MM/YYYY';

export const LOCAL_STORAGE_KEY = 'todo';