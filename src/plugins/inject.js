import Vue from 'vue';
import * as constants from '~/utils/constants';
import * as configs from '~/utils/configs';

Vue.prototype.$constants = constants;
Vue.prototype.$configs = configs;
