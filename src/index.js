import Vue from 'vue';
import App from "./App.vue";
import '@/plugins';
import '@/assets/scss/style.scss';

new Vue ({
  el: '#app',
  props: {
    title: 'Todo app'
  },
  render(h) {
    return h(App)
  }
})
